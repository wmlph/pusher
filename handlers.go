// pusher
// Copyright (c) 2014, opennota, All rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3.0 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library.

package pusher

// ErrorHandler is the type of error handler.
type ErrorHandler func(err error)

// ConnectHandler is the type of connect listener.
type ConnectHandler func()

// DisconnectHandler is the type of disconnect listener.
type DisconnectHandler func()
