// pusher
// Copyright (c) 2014, opennota, All rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3.0 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library.

package pusher

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	"github.com/opennota/sparse"
	"github.com/opennota/websocket"
)

// A Pusher represents the pusher service client.
type Pusher struct {
	apiKey  string
	options *Options
	url     string
	*websocket.Conn
	control                  chan struct{}
	controlLock              sync.Mutex
	channels                 map[string]int
	subscribers              map[string][]chan *Message
	subscribersToAllChannels []chan *Message
	errorHandler             ErrorHandler
	connectHandler           ConnectHandler
	disconnectHandler        DisconnectHandler
	state                    int
	caller                   *sparse.Sparse
}

// A Message represents the pusher message.
type Message struct {
	Event   string
	Channel string
	Data    json.RawMessage
}

// An Error represents the pusher error message.
type Error struct {
	Message string
	Code    int
}

func (e Error) Error() string {
	return e.Message
}

const (
	STATE_DISCONNECTED = iota
	STATE_CONNECTED

	SUBSCRIPTION_REQUESTED
	SUBSCRIPTION_SUCCEEDED
)

// New creates and returns an instance of Pusher.
func New(apiKey string, options *Options) *Pusher {
	if options == nil {
		options = &Options{}
	}
	return &Pusher{
		apiKey:      apiKey,
		options:     options,
		url:         options.buildURL(apiKey),
		channels:    make(map[string]int),
		subscribers: make(map[string][]chan *Message),
		state:       STATE_DISCONNECTED,
		caller:      sparse.New(10, time.Second),
	}
}

// Connect connects the client to the pusher service.
func (p *Pusher) Connect() error {
	p.controlLock.Lock()
	if p.state != STATE_DISCONNECTED {
		p.controlLock.Unlock()
		return nil
	}
	p.controlLock.Unlock()

	var err error
	p.Conn, err = websocket.Dial(p.url, "", "http://localhost/")
	if err != nil {
		return err
	}

	p.controlLock.Lock()
	p.control = make(chan struct{})
	p.controlLock.Unlock()

	go p.listen()

	return nil
}

// Close closes the pusher connection.
func (p *Pusher) Close() {
	p.controlLock.Lock()
	defer p.controlLock.Unlock()
	if p.state != STATE_DISCONNECTED {
		close(p.control)
		p.Conn.Close()
		p.state = STATE_DISCONNECTED
		if p.disconnectHandler != nil {
			p.disconnectHandler()
		}
	}
}

// Subscribe subscribes to the pusher channel(s).
func (p *Pusher) Subscribe(channels ...string) {
	for _, channel := range channels {
		p.controlLock.Lock()
		p.channels[channel] = SUBSCRIPTION_REQUESTED
		p.controlLock.Unlock()

		p.caller.Call(func() {
			websocket.Message.Send(
				p.Conn,
				fmt.Sprintf(`{"event":"pusher:subscribe","data":{"channel":"%s"}}`, channel),
			)
		})
	}
}

// Unsubscribe unsubscribes from the pusher channel(s).
func (p *Pusher) Unsubscribe(channels ...string) {
	for _, channel := range channels {
		p.caller.Call(func() {
			websocket.Message.Send(
				p.Conn,
				fmt.Sprintf(`{"event":"pusher:unsubscribe","data":{"channel":"%s"}}`, channel),
			)
		})

		p.controlLock.Lock()
		delete(p.channels, channel)
		p.controlLock.Unlock()
	}
}

// Bind returns a Go channel, that will be receiving all the messages sent to the pusher channel.
func (p *Pusher) Bind(channel string) chan *Message {
	p.controlLock.Lock()
	defer p.controlLock.Unlock()

	if _, ok := p.channels[channel]; !ok {
		p.controlLock.Unlock()
		p.Subscribe(channel)
		p.controlLock.Lock()
	}

	ch := make(chan *Message)
	p.subscribers[channel] = append(p.subscribers[channel], ch)
	return ch
}

// BindAll returns a Go channel, that will be receiving all the messages for all the pusher channels subscribed to.
func (p *Pusher) BindAll() chan *Message {
	p.controlLock.Lock()
	defer p.controlLock.Unlock()
	ch := make(chan *Message)
	p.subscribersToAllChannels = append(p.subscribersToAllChannels, ch)
	return ch
}

// OnError registers the error handler.
func (p *Pusher) OnError(h ErrorHandler) {
	p.controlLock.Lock()
	p.errorHandler = h
	p.controlLock.Unlock()
}

// OnConnect registers the connect listener.
func (p *Pusher) OnConnect(h ConnectHandler) {
	p.controlLock.Lock()
	p.connectHandler = h
	p.controlLock.Unlock()
}

// OnDisconnect registers the disconnect listener.
func (p *Pusher) OnDisconnect(h DisconnectHandler) {
	p.controlLock.Lock()
	p.disconnectHandler = h
	p.controlLock.Unlock()
}

func (p *Pusher) listen() {
	for {
		var msg Message
		err := websocket.JSON.Receive(p.Conn, &msg)
		if err != nil {
			p.Conn.Close()

			p.controlLock.Lock()
			if p.state != STATE_DISCONNECTED {
				p.state = STATE_DISCONNECTED
				if p.disconnectHandler != nil {
					p.disconnectHandler()
				}
			}
			p.controlLock.Unlock()
			return
		}

		switch msg.Event {
		case "pusher:connection_established":
			p.controlLock.Lock()
			p.state = STATE_CONNECTED
			if p.connectHandler != nil {
				p.connectHandler()
			}
			p.controlLock.Unlock()

		case "pusher:error":
			var e Error
			err := json.Unmarshal(msg.Data, &e)
			if err != nil {
				log.Println("can't unmarshal error")
			} else {
				p.controlLock.Lock()
				if p.errorHandler != nil {
					p.errorHandler(e)
				}
				p.controlLock.Unlock()
			}

		case "pusher_internal:subscription_succeeded":
			p.controlLock.Lock()
			p.channels[msg.Channel] = SUBSCRIPTION_SUCCEEDED
			p.controlLock.Unlock()

		case "pusher:ping":
			p.caller.Call(func() {
				websocket.Message.Send(p.Conn, `{"event":"pusher:pong","data":"{}"}`)
			})

		case "pusher_internal:member_added":

		case "pusher_internal:member_removed":

		default:
			if !strings.HasPrefix(msg.Event, "pusher:") && !strings.HasPrefix(msg.Event, "pusher_internal:") {
				// Try to do double decoding
				var str string
				err := json.NewDecoder(bytes.NewReader(msg.Data)).Decode(&str)
				if err == nil {
					msg.Data = []byte(str)
				}

				p.controlLock.Lock()
				for _, ch := range p.subscribers[msg.Channel] {
					data := make([]byte, len(msg.Data))
					copy(data, msg.Data)
					ch <- &Message{msg.Event, msg.Channel, data}
				}
				for _, ch := range p.subscribersToAllChannels {
					data := make([]byte, len(msg.Data))
					copy(data, msg.Data)
					ch <- &Message{msg.Event, msg.Channel, data}
				}
				p.controlLock.Unlock()
			}
		}

		select {
		case <-p.control:
			return
		default:
		}
	}
}
