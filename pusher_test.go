package pusher

import (
	"encoding/json"
	"fmt"
	"net"
	"net/http/httptest"
	"strconv"
	"testing"
	"time"

	"golang.org/x/net/websocket"
	. "gopkg.in/check.v1"
)

func Test(t *testing.T) { TestingT(t) }

type PusherTestSuite struct{}

var _ = Suite(&PusherTestSuite{})

var (
	msgConnectionEstablished       = `{"event":"pusher:connection_established"}`
	msgPing                        = `{"event":"pusher:ping"}`
	msgPong                        = `{"event":"pusher:pong"}`
	msgSubscriptionSucceededFormat = `{"event":"pusher_internal:subscription_succeeded","channel":"%s"}`
	msgErrorFormat                 = `{"event":"pusher:error","data":{"message":"%s","code":%d}}`
	msgMessageFormat               = `{"event":"message","channel":"%s","data":"%d"}`
)

func receiveLoop(ws *websocket.Conn, msgChan chan<- *Message, errorChan chan<- error) {
	for {
		var msg Message
		err := websocket.JSON.Receive(ws, &msg)
		if err != nil {
			errorChan <- err
			return
		}
		msgChan <- &msg
	}
}

func newServer(handler websocket.Handler) (*httptest.Server, string, int, error) {
	server := httptest.NewServer(handler)
	host, strport, err := net.SplitHostPort(server.Listener.Addr().String())
	if err != nil {
		server.Close()
		return nil, "", 0, err
	}
	port, err := strconv.Atoi(strport)
	if err != nil {
		server.Close()
		return nil, "", 0, err
	}
	return server, host, port, nil
}

func newClientServerPair(handler websocket.Handler) (*Pusher, *httptest.Server, error) {
	server, host, port, err := newServer(handler)
	if err != nil {
		return nil, nil, err
	}
	options := Options{Host: host, WSPort: port}
	client := New("app-key", &options)
	err = client.Connect()
	if err != nil {
		return nil, nil, err
	}
	return client, server, nil
}

func (s *PusherTestSuite) TestOnConnect(c *C) {
	onConnectServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)
	}
	client, server, err := newClientServerPair(websocket.Handler(onConnectServer))
	if err != nil {
		c.Error(err)
	}
	okChan := make(chan bool)
	client.OnConnect(func() { okChan <- true })
	select {
	case <-okChan:

	case <-time.After(time.Second):
		c.Error("timeout")
	}
	client.Close()
	server.Close()
}

func (s *PusherTestSuite) TestOnDisconnect(c *C) {
	disconnectServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)
	}
	client, server, err := newClientServerPair(websocket.Handler(disconnectServer))
	if err != nil {
		c.Error(err)
	}
	okChan := make(chan bool)
	client.OnDisconnect(func() { okChan <- true })
	select {
	case <-okChan:

	case <-time.After(time.Second):
		c.Error("timeout")
	}
	client.Close()
	server.Close()
}

func (s *PusherTestSuite) TestOnError(c *C) {
	errorServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)
		websocket.Message.Send(ws, fmt.Sprintf(msgErrorFormat, "Application disabled", 4003))
	}
	client, server, err := newClientServerPair(websocket.Handler(errorServer))
	if err != nil {
		c.Error(err)
	}
	okChan := make(chan bool)
	client.OnError(func(err error) {
		e := err.(Error)
		if e.Code == 4003 && e.Message == "Application disabled" {
			okChan <- true
		}
	})
	select {
	case <-okChan:

	case <-time.After(time.Second):
		c.Error("timeout")
	}
	client.Close()
	server.Close()
}

func (s *PusherTestSuite) TestPingPong(c *C) {
	okChan := make(chan bool)
	errorChan := make(chan error)
	pingServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)
		websocket.Message.Send(ws, msgPing)
		var msg Message
		err := websocket.JSON.Receive(ws, &msg)
		if err != nil {
			errorChan <- err
		}
		if msg.Event == "pusher:pong" {
			okChan <- true
		}
	}
	client, server, err := newClientServerPair(websocket.Handler(pingServer))
	if err != nil {
		c.Error(err)
	}
	select {
	case <-okChan:

	case err := <-errorChan:
		c.Error(err)
	case <-time.After(time.Second):
		c.Error("timeout")
	}
	client.Close()
	server.Close()
}

func (s *PusherTestSuite) TestSubscribe(c *C) {
	okChan := make(chan bool)
	subscribeServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)

		msgChan := make(chan *Message)
		errorChan := make(chan error)
		go receiveLoop(ws, msgChan, errorChan)

		for {
			select {
			case msg := <-msgChan:
				switch msg.Event {
				case "pusher:subscribe":
					var str string
					err := json.Unmarshal(msg.Data, &str)
					if err == nil {
						msg.Data = []byte(str)
					}
					data := struct{ Channel string }{}
					err = json.Unmarshal(msg.Data, &data)
					if err == nil && data.Channel == "my-channel" {
						okChan <- true
					}
				}
			case <-errorChan:
				return
			}
		}
	}

	client, server, err := newClientServerPair(websocket.Handler(subscribeServer))
	if err != nil {
		c.Error(err)
	}

	client.Subscribe("my-channel")

	select {
	case <-okChan:

	case <-time.After(time.Second):
		c.Error("timeout")
	}
	client.Close()
	server.Close()
}

func (s *PusherTestSuite) TestBind(c *C) {
	bindServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)

		msgChan := make(chan *Message)
		errorChan := make(chan error)
		go receiveLoop(ws, msgChan, errorChan)

		subscribed := false
		i := 0
		ticker := time.NewTicker(time.Second)
		for {
			select {
			case msg := <-msgChan:
				switch msg.Event {
				case "pusher:subscribe":
					var str string
					err := json.Unmarshal(msg.Data, &str)
					if err == nil {
						msg.Data = []byte(str)
					}
					data := struct{ Channel string }{}
					err = json.Unmarshal(msg.Data, &data)
					if err == nil && data.Channel == "my-channel" {
						websocket.Message.Send(ws, fmt.Sprintf(msgSubscriptionSucceededFormat, data.Channel))
						subscribed = true
					}
				}
			case <-ticker.C:
				if subscribed {
					websocket.Message.Send(ws, fmt.Sprintf(msgMessageFormat, "my-channel", i))
					i++
					if i == 3 {
						ticker.Stop()
						return
					}
				}
			case <-errorChan:
				return
			}
		}
	}

	client, server, err := newClientServerPair(websocket.Handler(bindServer))
	if err != nil {
		c.Error(err)
	}

	ch := client.Bind("my-channel")

	timer := time.After(4 * time.Second)

	messages := []string{}
L:
	for i := 0; i < 3; i++ {
		select {
		case msg := <-ch:
			if msg.Channel == "my-channel" && msg.Event == "message" {
				messages = append(messages, string(msg.Data))
			}
		case <-timer:
			break L
		}
	}
	c.Check(messages, DeepEquals, []string{"0", "1", "2"})
	client.Close()
	server.Close()
}

func (s *PusherTestSuite) TestBindAll(c *C) {
	bindServer := func(ws *websocket.Conn) {
		websocket.Message.Send(ws, msgConnectionEstablished)

		msgChan := make(chan *Message)
		errorChan := make(chan error)
		go receiveLoop(ws, msgChan, errorChan)

		subscribed := 0
		i := 0
		ticker := time.NewTicker(time.Second)
		for {
			select {
			case msg := <-msgChan:
				switch msg.Event {
				case "pusher:subscribe":
					var str string
					err := json.Unmarshal(msg.Data, &str)
					if err == nil {
						msg.Data = []byte(str)
					}
					data := struct{ Channel string }{}
					err = json.Unmarshal(msg.Data, &data)
					if err == nil && (data.Channel == "my-channel.1" || data.Channel == "my-channel.2") {
						websocket.Message.Send(ws, fmt.Sprintf(msgSubscriptionSucceededFormat, data.Channel))
						subscribed++
					}
				}
			case <-ticker.C:
				if subscribed == 2 {
					websocket.Message.Send(ws, fmt.Sprintf(msgMessageFormat, "my-channel.1", i))
					websocket.Message.Send(ws, fmt.Sprintf(msgMessageFormat, "my-channel.2", i*2))
					i++
					if i == 3 {
						ticker.Stop()
						return
					}
				}
			case <-errorChan:
				return
			}
		}
	}

	client, server, err := newClientServerPair(websocket.Handler(bindServer))
	if err != nil {
		c.Error(err)
	}

	client.Subscribe("my-channel.1", "my-channel.2")
	ch := client.BindAll()

	timer := time.After(4 * time.Second)

L:
	for i := 0; i < 2; i++ {
		select {
		case msg := <-ch:
			c.Check(msg.Channel, Equals, fmt.Sprintf("my-channel.%d", i+1))
		case <-timer:
			c.Error("timeout")
			break L
		}
	}
	client.Close()
	server.Close()
}
