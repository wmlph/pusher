// pusher
// Copyright (c) 2014, opennota, All rights reserved.
//
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 3.0 of the License, or (at your option) any later version.
//
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with this library.

package pusher

import (
	"fmt"
)

var (
	PUSHER_PROTOCOL_VERSION = "7"
	HOST                    = "ws.pusherapp.com"
	WS_SCHEME               = "ws://"
	WSS_SHEME               = "wss://"
	WS_PORT                 = 80
	WSS_PORT                = 443
	URI_SUFFIX              = "?client=go&protocol=" + PUSHER_PROTOCOL_VERSION
)

// An Options object is passed to the pusher.New function.
type Options struct {
	Host      string
	WSPort    int
	WSSPort   int
	Encrypted bool
}

func (o Options) buildURL(apiKey string) string {
	var scheme string
	var port int
	if o.Encrypted {
		scheme = WSS_SHEME
		port = o.WSSPort
		if port == 0 {
			port = WSS_PORT
		}
	} else {
		scheme = WS_SCHEME
		port = o.WSPort
		if port == 0 {
			port = WS_PORT
		}
	}
	host := o.Host
	if host == "" {
		host = HOST
	}
	return fmt.Sprintf("%s%s:%d/app/%s%s", scheme, host, port, apiKey, URI_SUFFIX)
}
