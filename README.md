Pusher [![Build Status](https://travis-ci.org/opennota/pusher.png?branch=master)](https://travis-ci.org/opennota/pusher)
======

Pusher is a simple [pusher.com](http://pusher.com/) client.

## Installation

    $ go get github.com/opennota/pusher

## Usage

```go
	options := pusher.Options{Encrypted: true}
	p := pusher.New("APP_KEY", &options)
	p.OnConnect(func() {
		fmt.Println("connected")
	})
	p.OnDisconnect(func() {
		fmt.Println("disconnected")
	})
	p.OnError(func(err error) {
		e := err.(pusher.Error)
		fmt.Println("error:", e.Code, e.Message)
	})
	err := p.Connect()
	if err != nil {
		log.Fatal(err)
	}
	ch := p.Bind("my-channel")

	timer := time.After(10 * time.Second)
L:
	for {
		select {
		case msg := <-ch:
			fmt.Println("message", msg.Data)
		case <-timer:
			fmt.Println("timeout!")
			break L
		}
	}
	p.Close()
```

## Documentation

http://godoc.org/github.com/opennota/pusher

## License

GNU LGPL v3+

